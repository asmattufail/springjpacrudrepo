package com.zetcode.controller;

import com.zetcode.model.Country;
import com.zetcode.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
public class CountryController {

    @Autowired
    private CountryService service;

    @RequestMapping("/country/findCountry")
    public Country findCountry(@RequestParam("name") String countryName){
        Country country = new Country();
        country.setName(countryName);
        return service.findByName(countryName);
    }

    @RequestMapping(value = "/country/save", method = RequestMethod.PUT)
    public Country save(@RequestBody Country country){
        String countryName = country.getName();
        RestTemplate restTemplate = new RestTemplate();
        Country[] countryObj = restTemplate.getForObject("https://restcountries.eu/rest/v2/name/{countryName}", Country[].class, countryName);
        return service.save(countryObj[0]);
    }
}

