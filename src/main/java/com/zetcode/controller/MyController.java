package com.zetcode.controller;

import com.zetcode.model.User;
import com.zetcode.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class MyController {

    @Autowired
    private UserService service;

    @GetMapping("/users")
    public List<User> allUsers(){
        return service.findAll();
    }

    @GetMapping("/users/{id}")
    public Optional<User> findById(@PathVariable String id){
        Long userId = Long.parseLong(id);
        return service.findById(userId);
    }

    @GetMapping("/users/count")
    public Long getCount(){
        return service.count();
    }

    @DeleteMapping("/users/{id}")
    public void delete(@PathVariable String id) {
        Long userId = Long.parseLong(id);
        service.deleteById(userId);
    }

    @PostMapping("/users/save")
    public User save(@RequestBody User user){
        return service.save(user);
    }
}

