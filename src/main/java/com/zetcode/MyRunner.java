package com.zetcode;

import com.zetcode.model.Country;
import com.zetcode.model.User;
import com.zetcode.repository.CountryRepository;
import com.zetcode.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class MyRunner implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(MyRunner.class);
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CountryRepository countryRepository;

    @Override
    public void run(String... args) throws Exception {
        logger.info("initializing users");

        User u1 = new User("Burak", "Ozcivit", "burak.ozcivit@gmail.com");
        userRepository.save(u1);

        User u2 = new User("Robert", "Black", "rb34@gmail.com");
        userRepository.save(u2);

        User u3 = new User("John", "Doe", "jdoe@gmail.com");
        userRepository.save(u3);

        Country c1 = new Country("Turkey", "Ankara");
        countryRepository.save(c1);

        Country c2 = new Country("Afghanistan", "Kabul");
        countryRepository.save(c2);
    }
}

