package com.zetcode.service;

import com.zetcode.model.Country;
import com.zetcode.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CountryService {

    @Autowired
    private CountryRepository repository;

    public Country save(Country country){
        return repository.save(country);
    }

    public Country findByName(String countryName){
        return repository.findByName(countryName);
    }
}

