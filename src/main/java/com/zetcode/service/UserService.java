package com.zetcode.service;

import com.zetcode.model.User;
import com.zetcode.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public List<User> findAll(){
        Iterable itr = userRepository.findAll();
        List<User> users = new ArrayList();
        itr.forEach(e -> users.add((User) e));
        return users;
    }

    public Long count() {
        return userRepository.count();
    }

    public void deleteById(Long userId) {
        userRepository.deleteById(userId);
    }

    public User save(User user){
        return userRepository.save(user);
    }

    public Optional<User> findById(Long userId){
        return userRepository.findById(userId);
    }

}

